import { useState } from 'react';
import Expenses from './components/Expenses/Expenses';
import NewExpense from './components/NewExpense/NewExpense';

const App = () => {
    const [expenses, setExpenses] = useState([]);

    /**
     * @param {any} newExpense
     */
    const addExpenseHandler = (newExpense) => {
        setExpenses((prevExpenses) => {
            return [newExpense, ...prevExpenses];
        });
    };

    return (
        <div>
            <NewExpense onAddExpense={addExpenseHandler} />
            <Expenses items={expenses} />
        </div>
    );
};

export default App;

import './Card.css';

/**
 * @param {{ className: string; children: import("react").ReactNode; }} props
 */
const Card = (props) => {
    const classes = 'card ' + props.className;
    return <div className={classes}>{props.children}</div>;
};

export default Card;

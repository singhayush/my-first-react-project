import './NewExpense.css';
import ExpenseForm from './ExpenseForm';
import { useState } from 'react';

/**
 * @param {{ onAddExpense: (arg0: any) => void; }} props
 */
const NewExpense = (props) => {
    const [isEditing, setIsEditing] = useState(false);

    /**
     * @param {any} expenseData
     */
    const saveExpenseHandler = (expenseData) => {
        const newExpense = {
            ...expenseData,
            _id: (Math.random() * 10000000000).toFixed(),
        };

        props.onAddExpense(newExpense);
        setIsEditing(false);
    };

    const startEditingHandler = () => {
        setIsEditing(true);
    };

    const stopEditingHandler = () => {
        setIsEditing(false);
    };

    return (
        <div className="new-expense">
            {!isEditing && <button onClick={startEditingHandler}>Add New Expense</button>}
            {isEditing && (
                <ExpenseForm onSaveExpense={saveExpenseHandler} onReset={stopEditingHandler} />
            )}
        </div>
    );
};

export default NewExpense;

import { useState } from 'react';
import './ExpenseForm.css';

/**
 * @param {{ onSaveExpense: (arg0: { date: Date; title: string; amount: string; }) => void; onReset: () => void; }} props
 */
const ExpenseForm = (props) => {
    const [userInput, setUserInput] = useState({
        title: '',
        date: '',
        amount: '',
    });

    /**
     * @param {{ target: { value: any; }; }} event
     */
    const titleChangeHandler = (event) => {
        setUserInput((prevState) => {
            return {
                ...prevState,
                title: event.target.value,
            };
        });
    };

    /**
     * @param {{ target: { value: any; }; }} event
     */
    const dateChangeHandler = (event) => {
        setUserInput((prevState) => {
            return {
                ...prevState,
                date: event.target.value,
            };
        });
    };

    /**
     * @param {{ target: { value: any; }; }} event
     */
    const amountChangeHandler = (event) => {
        setUserInput((prevState) => {
            return {
                ...prevState,
                amount: event.target.value,
            };
        });
    };

    /**
     * @param {{ preventDefault: () => void; }} event
     */
    const submitHandler = (event) => {
        event.preventDefault();

        const expenseData = {
            ...userInput,
            date: new Date(userInput.date),
        };

        props.onSaveExpense(expenseData);

        setUserInput({
            title: '',
            date: '',
            amount: '',
        });
    };

    const resetHandler = () => {
        props.onReset();
    };

    return (
        <form onSubmit={submitHandler} onReset={resetHandler}>
            <div className="new-expense_controls">
                <div className="new-expense_control">
                    <label>Title</label>
                    <input type="text" value={userInput.title} onChange={titleChangeHandler} />
                </div>
                <div className="new-expense_control">
                    <label>Amount</label>
                    <input
                        type="number"
                        min="0.01"
                        step="0.01"
                        value={userInput.amount}
                        onChange={amountChangeHandler}
                    />
                </div>
                <div className="new-expense_control">
                    <label>Date</label>
                    <input
                        type="date"
                        min="2019-01-01"
                        max="2025-12-31"
                        value={userInput.date}
                        onChange={dateChangeHandler}
                    />
                </div>
            </div>
            <div className="new-expense_actions">
                <button type="submit">Add Expense</button>
                <button type="reset">Cancel</button>
            </div>
        </form>
    );
};

export default ExpenseForm;

import './ExpensesFilter.css';

/**
 * @param {{ onChangeFilter: (arg0: any) => void; selected: string | number | readonly string[]; }} props
 */
const ExpensesFilter = (props) => {
    /**
     * @param {{ target: { value: any; }; }} event
     */
    const dropdownChangeHandler = (event) => {
        props.onChangeFilter(event.target.value);
    };

    return (
        <div className="expenses-filter">
            <div className="expenses-filter_control">
                <label>Filter By Year</label>
                <select value={props.selected} onChange={dropdownChangeHandler}>
                    <option value="2022">2022</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                </select>
            </div>
        </div>
    );
};

export default ExpensesFilter;
